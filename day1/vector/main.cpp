#include <iostream>
#include <vector>

using namespace std;

class MyClass
{
    int id_;
public:
    MyClass(int id) :id_(id){
        cout << "Constructor" << endl;
    }

    // copying
    MyClass(const MyClass& my) = delete;
//    {
//        cout << "Copy Constructor" << endl;
//    }

    MyClass operator=(const MyClass& my) = delete;
//    {
//        cout << "Copy Assignment" << endl;
//    }

    // C++11 moving
    MyClass(MyClass&& my) noexcept
    {
        cout << "Move Constructor" << endl;
    }

    MyClass operator=(MyClass&& my) noexcept
    {
        cout << "Move Assignment" << endl;
    }

    ~MyClass()
    {
        cout << "Destructor" << endl;
    }
};

int main()
{
    cout << "Vector" << endl;
    vector<int> v;
    cout << "size: " << v.size() << endl;
    //cout << "max_size: " << v.max_size() << endl;
    v.push_back(10);
    v.push_back(20);
    cout << "size: " << v.size() << endl;
    cout << "capacity: " << v.capacity() << endl;
    v.push_back(30);
    cout << "size: " << v.size() << endl;
    cout << "capacity: " << v.capacity() << endl;
    v.shrink_to_fit();
    cout << "size: " << v.size() << endl;
    cout << "capacity: " << v.capacity() << endl;

    vector<int> v2;
    v2.reserve(8);
    v2.push_back(10);
    cout << "size 2: " << v2.size() << endl;
    cout << "capacity 2: " << v2.capacity() << endl;
    cout << "elements = ";
    for (auto& el : v2)
    {
        cout << el << " ";
    }
    cout << endl;

    vector<int> v3;
    v3.resize(8);
    v3.push_back(10);
    cout << "size 3: " << v3.size() << endl;
    cout << "capacity 3: " << v3.capacity() << endl;
    cout << "elements = ";
    //for (auto& el : v3)
    //vector<int>::const_iterator it;
    //for (auto it = v3.cbegin() ; it != v3.cend(); ++it)
    for (auto it = begin(v2) ; it != end(v3); ++it)
    {
        cout << *it << " ";
    }
    cout << endl;

    // Class
    vector<MyClass> vmy;
    //vmy.push_back(MyClass(1));
    //vmy.push_back(MyClass(2));
    vmy.emplace_back(1);
    vmy.emplace_back(2);

    vector<int> v4{1,2,3,4,5};
    vector<int> v5(v3.begin(), v3.begin()+3);

    return 0;    
}

