#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <cassert>
#include <list>
#include <functional>

using namespace std;

void avg(int array[], size_t size)
{
    double sum = 0.0;
    for(size_t i = 0; i < size; ++i)
        sum += array[i];

    cout << "AVG: " << sum / size << endl;
}

bool is_even(int n)
{
    return n % 2 == 0;
}

int main()
{
    int tab[100];

    for(int i = 0; i < 100; ++i)
        tab[i] = rand() % 100;

    cout << "tab: ";
    for(int i = 0; i < 100; ++i)
        cout << tab[i] << " ";
    cout << "\n\n";

    // 1 - utwórz wektor vec_int zawierający kopie wszystkich elementów z  tablicy tab
    //vector<int> vec_int(tab, tab+100);
    vector<int> vec_int(begin(tab), end(tab));
    for(int& el : vec_int) cout << el << " ";
    cout << endl;

    // 2 - wypisz wartość średnią przechowywanych liczb w vec_int
    avg(&vec_int[0], vec_int.size());
    avg(vec_int.data(), vec_int.size()); // C++11 and up

    // 3a - utwórz kopię wektora vec_int o nazwie vec_cloned
    vector<int> vec_cloned(vec_int);
    //vector<int> vec_cloned(move(vec_int));
    //vector<int> vec_cloned;
    //vec_cloned.swap(vec_int);

    //printing
    for(int& el : vec_int) cout << el << " ";
    cout << endl;
    for(int& el : vec_cloned) cout << el << " ";
    cout << endl;

    // 3b - wyczysć kontener vec_int i zwolnij pamięć po buforze wektora
    vec_int.clear();
    //vector<int>().swap(vec_int); //old
    vec_int.shrink_to_fit();
    cout << "Capacity and size: " << vec_int.capacity()
         << " " << vec_int.size() << endl;

    int rest[] = { 1, 2, 3, 4 };
    // 4 - dodaj na koniec wektora vec_cloned zawartość tablicy rest
    for(auto& el : rest)
        vec_cloned.push_back(el);
    vec_cloned.insert(vec_cloned.end(), begin(rest), end(rest));
    vec_cloned.insert(vec_cloned.end(), rest, rest+4);

    for(int& el : vec_cloned) cout << el << " ";
    cout << endl;

    // 5 - posortuj zawartość wektora vec_cloned
    sort(begin(vec_cloned), end(vec_cloned));

    for(int& el : vec_cloned) cout << el << " ";
    cout << endl;

    // 6 - wyświetl na ekranie zawartość vec_cloned za pomocą iteratorów
    cout << "\nvec_cloned: ";
    // TODO
    cout << "\n\n";

    // 7 - utwórz kontener numbers (wybierz odpowiedni typ biorąc pod uwagę następne polecenia) zawierający kopie elementów vec_cloned
    list<int> numeric(vec_cloned.begin(), vec_cloned.end());
    cout << "List numeric: ";
    for(int& el : numeric) cout << el << " ";
    cout << endl;

    // 8 - usuń duplikaty elementów przechowywanych w kontenerze
    numeric.unique();

    cout << "List numeric - after unique: ";
    for(int& el : numeric) cout << el << " ";
    cout << endl;

    // 9 - usuń liczby parzyste z kontenera

    int n = 3;
    auto pred = [n] (int item) { return item%n; };
    function<bool(int)> fun = pred;
    numeric.remove_if( fun );

    cout << "List numeric - after is_even: ";
    for(int& el : numeric) cout << el << " ";
    cout << endl;

    // 10 - wyświetl elementy kontenera w odwrotnej kolejności
    cout << "numbers: ";
    // TODO
    cout << "\n\n";

    // 11 - skopiuj dane z numbers do listy lst_numbers jednocześnie i wyczyść kontener numbers
    list<int> lst_number(move(numeric));

    // 12 - wyświetl elementy kontenera lst_numbers w odwrotnej kolejności
    cout << "lst_numbers: ";
    // TODO
    cout << "\n\n";    
}

