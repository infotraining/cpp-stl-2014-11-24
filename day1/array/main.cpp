#include <iostream>
#include <string>
#include <string.h>
#include <array> // C++11

using namespace std;

int main()
{
    cout << "STL Array" << endl;

    std::array<int, 5> arr{ 1,2,3,4,5 };
    //arr.fill(10);

    for(auto& el : arr)
    {
        cout << el << " ";
    }

    cout << "element 1: " << arr[5] << endl;
    try
    {
        cout << "element 1: " << arr.at(5) << endl;
    }
    catch(...)
    {
        cout << "Got exception" << endl;
    }


    array<char, 100> buff{};

    strcpy(buff.data(), "ala ma kota");
    cout << endl;
    cout << buff.data() << endl;

    array<string, 3> three_strings{"ala", "ma", "kota"};

    cout << std::get<1>(three_strings) << endl;
    cout << endl;

    return 0;
}

