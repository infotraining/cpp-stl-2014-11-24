#include <iostream>
#include <iterator>
#include <algorithm>
#include <vector>
#include <deque>
#include <list>

using namespace std;

int main()
{
    cout << "Iterators" << endl;

    vector<int> v{7,6,3,4,5};

    cout << "Vec: " << endl;

    sort(next(v.begin()), v.end());

    for(auto it = v.cbegin() ; it != v.cend() ; it = next(it))
        cout << *it << " ";
    cout << endl;

    vector<int> res;
    back_insert_iterator<vector<int>> bi(res);
    copy(v.begin(), v.end(), bi);

    for(auto& el : res)
        cout << el << " ";
    cout << endl;

    istream_iterator<string> start(cin);
    istream_iterator<string> end;

    vector<string> input(start, end);

    for(auto& el : input) cout << el << " > ";
    cout << endl;

    copy(input.cbegin(), input.cend(), ostream_iterator<string>(cout, " > "));
    cout << endl;

    return 0;
}

