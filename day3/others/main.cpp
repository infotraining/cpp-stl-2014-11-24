#include <iostream>
#include <vector>
#include <bitset>

using namespace std;

int main()
{
    vector<bool> v;
    v.push_back(true);
    v.push_back(false);
    for(auto el : v)
        cout << el << endl;

    bitset<800> a;
    a[0] = 1;
    cout << a.to_string() << endl;
    return 0;

}

