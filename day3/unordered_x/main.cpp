#include <iostream>
#include <unordered_set>
#include <chrono>
#include <boost/functional/hash.hpp>
#include <set>

using namespace std;

struct Point
{
    int x, y;
    Point(int x, int y) : x(x), y(y)
    {
    }

    bool operator==(const Point& other) const
    {
        return (x == other.x) && (y == other.y);
    }
    bool operator<(const Point& other) const
    {
        return x*100+y < (other.x*100+other.y) ;
    }
};

namespace std
{
    template<>
    struct hash<Point>
    {
        size_t operator()(const Point& p) const
        {
            //std::hash<int> hasher;
            size_t seed{};
            boost::hash_combine(seed, p.x);
            boost::hash_combine(seed, p.y);
            // return hasher(p.x + p.y*100);
            // scout << seed << " ";
            return seed;
        }
    };
}

int main()
{
    //unordered_set<Point> points;
    set<Point> points;
    auto start = chrono::high_resolution_clock::now();
    for (int i = 0 ; i < 10000000 ; ++i)
    {
        points.emplace(rand() % 100, rand() % 100);
    }
    auto end1 = chrono::high_resolution_clock::now();

//    cout << "Bucket count " << points.bucket_count() <<endl;
//    cout << "Load Factor " << points.load_factor()<<endl;
//    cout << "max LF " << points.max_load_factor()<<endl;

    for (int i = 0 ; i < 10000 ; ++i)
    {
        points.find(Point(rand() % 100, rand() % 100));
    }
    auto end2 = chrono::high_resolution_clock::now();
    cout <<  chrono::duration_cast<chrono::milliseconds>(end1-start).count() << " ms, ";
    cout <<  chrono::duration_cast<chrono::microseconds>(end2-end1).count() << " us" << endl;
    return 0;
}

