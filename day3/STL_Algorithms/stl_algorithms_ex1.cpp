#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>

using namespace std;

class RandGen
{
    int range_;
public:
    RandGen(int range) : range_(range)
    {
    }

    int operator()() const
    {
        return rand() % range_;
    }
};

struct Avg
{
    double avg;
    int counter;
    Avg() : avg(0), counter(0) {}
    operator()(int n)
    {
        counter++;
        avg += n;
    }

    double get_avg()
    {
        return avg/counter;
    }
};

int main()
{
    vector<int> vec(25);

    generate(vec.begin(), vec.end(), RandGen(30));

    for( auto & el : vec)
        cout << el << " ";
    cout << endl;


    // 1a - wyświetl parzyste
    copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "),
            [] (int n) {return n%2 == 0;});
    cout << endl;


    // 1b - wyswietl ile jest nieparzystych
    cout << "Ilosc nieparzystych " <<
            count_if(vec.begin(), vec.end(),
                     [] (int n) { return n%2;});
    cout << endl;
    // 1c - wyswietl ile jest parzystych
    cout << "Ilosc parzystych " <<
            count_if(vec.begin(), vec.end(),
                     [] (int n) { return n%2 == 0;});
    cout << endl;

    int mul = 3;
    auto garbage_start = remove_if(vec.begin(), vec.end(),
                                   [mul] (int n) -> bool {return n%mul == 0 ;});
    vec.erase(garbage_start, vec.end());
    for( auto & el : vec)
        cout << el << " ";
    cout << endl;

    // 3 - tranformacja: podnieś liczby do kwadratu
    transform(vec.begin(), vec.end(), vec.begin(), [] (int n) { return n*n;});
    for( auto & el : vec)
        cout << el << " ";
    cout << endl;

    // 4 - wypisz 5 najwiekszych liczb
    nth_element(vec.begin(), next(vec.begin(),5), vec.end(), greater<int>());
    copy_n(vec.begin(), 5, ostream_iterator<int>(cout, " "));
    cout << endl;

    // 5 - policz wartosc srednia
    //cout << "avg: " << accumulate(vec.begin(), vec.end(), 0.0)/vec.size() << endl;
    cout << "avg: " << accumulate(vec.begin(), vec.end(), string(),
                                  [] (string a, int b) { return a + to_string(b) + " "; }) << endl;


    Avg avg = for_each(vec.begin(), vec.end(), Avg());
    cout << avg.get_avg() << endl;

    // 6 - wyswietl wszystkie mniejsze od 50
    // TODO
}
