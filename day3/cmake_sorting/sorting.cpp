// sorting.cpp
// written by Kazimierz Pomierny

#ifndef _WIN32_WINNT            // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINNT 0x0600     // Change this to the appropriate value to target other versions of Windows.
#endif

#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include <memory.h>

typedef char *LPSTR;
typedef const char *LPCSTR;

typedef unsigned int UINT;
typedef unsigned char BYTE, *LPBYTE;
typedef int BOOL;
#define TRUE  1
#define FALSE 0

class Timer
{
public:
    Timer()
    {}

    auto GetCounter() { return std::chrono::high_resolution_clock::now(); }
    template<typename T>
    long ToMicrosec( T nBeg, T nEnd)
    {
        return std::chrono::duration_cast<std::chrono::microseconds>(nEnd-nBeg).count();
    }
};

void Run(void);

typedef struct tag_WORDINFO
    {
    int  nPos;
    int  nLen;
    //UINT nHash;
    int  nCount;
    } WORDINFO, *PWORDINFO;

LPBYTE g_pBufInp = NULL;
int g_nTotalWords = 0;
int g_nUsedWords  = 0;
int g_nMaxWords   = 0;
int g_nFileSize   = 0;

PWORDINFO g_pWI = NULL;
//----------------------------------------------------------------------------|
PWORDINFO GetWordInfo(void)
{
if (g_nUsedWords < g_nMaxWords)
    {
    return g_pWI + g_nUsedWords++;
    }

int nNewSize = g_nTotalWords + (g_nTotalWords >> 1);
if (!nNewSize) nNewSize = g_nFileSize / 8;

PWORDINFO pNewWI = new WORDINFO[nNewSize];
if (!pNewWI) return NULL;

if (g_nUsedWords && g_pWI)
    {
    memcpy((LPBYTE)pNewWI, (LPBYTE)g_pWI, g_nUsedWords * sizeof(WORDINFO));
    delete[] g_pWI;
    }

g_pWI = pNewWI;
g_nTotalWords = nNewSize;
g_nMaxWords = g_nTotalWords - 1;

return g_pWI + g_nUsedWords++;
}//---------------------------------------------------------------------------|
void Decode(void)
{
int nBegin = -1;
int nAct   = 0;
int nSize  = g_nFileSize;
int nWSize = 0;
UINT nHash;
BOOL bInWord = FALSE;
LPBYTE pbtAct = g_pBufInp;
BYTE btAct;
PWORDINFO pWI = NULL;

while (nSize)
    {
    btAct = *pbtAct;

    if ((btAct >= 'a' && btAct <= 'z') || (btAct >= 'A' && btAct <= 'Z'))
        {
        if (bInWord) 
            {
            ++nWSize;
            //nHash += btAct;
            }
        else
            {
            bInWord = TRUE;
            //nHash   = 0;
            nWSize  = 1;
            nBegin  = nAct;
            }
        }
    else
        {
        if (bInWord) 
            {
            bInWord = FALSE;
            pWI = GetWordInfo();
            if (!pWI) return;

            pWI->nPos  = nBegin;
            pWI->nLen  = nWSize;
            //pWI->nHash = nHash ;
            pWI->nCount = 0;
            }
        }
    ++pbtAct;
    ++nAct;
    --nSize;
    }
}//---------------------------------------------------------------------------|
int Compare1(const void *elem1, const void *elem2)
{
PWORDINFO pWI1 = (PWORDINFO)elem1;
PWORDINFO pWI2 = (PWORDINFO)elem2;

LPBYTE pAct1 = g_pBufInp + pWI1->nPos;
LPBYTE pAct2 = g_pBufInp + pWI2->nPos;
int nLen1 = pWI1->nLen;
int nLen2 = pWI2->nLen;
//int nLen = (nLen1 < nLen2) ? nLen1 : nLen2;

while (nLen1 && nLen2)
//while (nLen)
    {
    if (*pAct1 != *pAct2) return (int)*pAct1 - (int)*pAct2;

    //--nLen;
    --nLen1;
    --nLen2;
    ++pAct1;
    ++pAct2;
    }

if (nLen1) return -1;
if (nLen2) return  1;
return 0;

//if (nLen1 == nLen2) return 0;
//return nLen1 - nLen2;
}//---------------------------------------------------------------------------|
int Compare2(const void *elem1, const void *elem2)
{
PWORDINFO pWI1 = (PWORDINFO)elem1;
PWORDINFO pWI2 = (PWORDINFO)elem2;

return pWI2->nCount - pWI1->nCount;
}//---------------------------------------------------------------------------|
BOOL WordEqual(PWORDINFO pWI1, PWORDINFO pWI2)
{
if (pWI1->nLen  != pWI2->nLen ) return FALSE;
//if (pWI1->nHash != pWI2->nHash) return FALSE;

LPBYTE pAct1 = g_pBufInp + pWI1->nPos;
LPBYTE pAct2 = g_pBufInp + pWI2->nPos;
int nLen = pWI1->nLen;

while (nLen)
    {
    if (*pAct1 != *pAct2) return FALSE;

    --nLen;
    ++pAct1;
    ++pAct2;
    }

return TRUE;
}//---------------------------------------------------------------------------|
void Combine(void)
{
PWORDINFO pWIAct = g_pWI;
PWORDINFO pWINum = NULL;

for (int i = 0 ; i < g_nUsedWords ; i++, pWIAct++)
    {
    if (!pWINum)
        {
        if (pWIAct->nLen)
            {
            pWINum = pWIAct;
            pWINum->nCount = 1;
            continue;
            }
        else
            continue;
        }

    if (WordEqual(pWINum, pWIAct))
        pWINum->nCount += 1;
    else
        {
        pWINum = pWIAct;
        pWINum->nCount = 1;
        }
    }

qsort(g_pWI, g_nUsedWords, sizeof(WORDINFO), Compare2);
}//---------------------------------------------------------------------------|
void Run(void)
{
Timer cTimer;
LPCSTR pszFilename = "/home/leszek/swanns_way.txt";

auto nTimeBeg = cTimer.GetCounter();

FILE *pFile = fopen(pszFilename, "rb");
if (!pFile) return;

fseek(pFile, 0, SEEK_END);
g_nFileSize = ftell(pFile);

fseek(pFile, 0, SEEK_SET);

g_pBufInp = new BYTE[g_nFileSize];
if (!g_pBufInp) { fclose(pFile); return; }

int nRead = fread(g_pBufInp, 1, g_nFileSize, pFile);
fclose(pFile);


Decode();
if (!g_nUsedWords) return;

qsort(g_pWI, g_nUsedWords, sizeof(WORDINFO), Compare1);
PWORDINFO pWI;

auto nTimeMid = cTimer.GetCounter();

//INT64 nTimeMid = cTimer.GetCounter();

// draw sorted input
/*
pWI = g_pWI;
for (int i = 0 ; i < g_nUsedWords ; i++, pWI++)
    {
    if (i > 100) break;
    g_pBufInp[pWI->nPos + pWI->nLen] = '\0';
    printf("%s\n", g_pBufInp + pWI->nPos);
    }
*/
Combine();

auto nTimeEnd = cTimer.GetCounter();
int nFirst  = (int)cTimer.ToMicrosec(nTimeBeg, nTimeMid);
int nSecond = (int)cTimer.ToMicrosec(nTimeMid, nTimeEnd);
int nTotal  = (int)cTimer.ToMicrosec(nTimeBeg, nTimeEnd);

// draw results
pWI = g_pWI;
for (int i = 0 ; i < g_nUsedWords ; i++, pWI++)
    {
    if (!pWI->nCount)
        break;

    g_pBufInp[pWI->nPos + pWI->nLen] = '\0';
    printf("%4i %s\n", pWI->nCount, g_pBufInp + pWI->nPos);

    //if (i > 100) break;
    }

printf("FileLen = %i, TotalWords = %i\n", g_nFileSize, g_nUsedWords);
printf("FirstPass = %4.3fms, SecondPass = %4.3fms, Total = %4.3fms\n", (double)nFirst / 1000.0, (double)nSecond / 1000.0, (double)nTotal / 1000.0);
double dTotalSec = (double)nTotal / 1000000.0;

printf("%4.3f MegaBytes/s, %4.3f MegaWords/s\n", ((double)g_nFileSize / dTotalSec) / 1000000.0, ((double)g_nUsedWords / dTotalSec) / 1000000.0);

}//---------------------------------------------------------------------------|
int main(int argc, char* argv[])
{
Run();

if (g_pBufInp) delete[] g_pBufInp;
if (g_pWI) delete[] g_pWI;

return 0;
}//---------------------------------------------------------------------------|
