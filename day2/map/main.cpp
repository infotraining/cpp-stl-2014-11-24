#include <iostream>
#include <map>
#include <string>

using namespace std;

int main()
{
    map<string, double> stocks;

    stocks.insert(pair<string, double>("APPL", 12.34));
    stocks.insert(make_pair("GOGL", 32.2));

    cout << "APPL: " << stocks.at("APPL") << endl;
    //cout << "APPL: " << stocks.at("APP") << endl;

    stocks["NOKIA"] = 123.45;
    stocks["NOKIA"] = 13.45;
    stocks.insert(make_pair("GOGL", 0.2));


    cout << "NOKIA: " << stocks["NOKIA"] << endl;
    cout << "IBM: " << stocks["IBM"] << endl;

    for(auto& el : stocks)
    {
        cout << el.first << " : " << el.second << endl;
    }
    return 0;
}

