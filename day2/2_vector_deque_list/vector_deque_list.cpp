#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <ctime>
#include <algorithm>
#include <cstring>
#include <chrono>

using namespace std;

/*
    Utwórz trzy sekwencje typu int: vector, deque i list. Wypelnij je wartosciami losowymi.
    Porównaj czasy tworzenia i wypelniania danymi sekwencji. Napisz szablon funkcji sortującej sekwencje
    vector i deque. Napisz specjalizowany szablon funkcji realizującej sortowanie dla kontenera list.
    Porównaj efektywność operacji sortowania.
*/

// szablony funkcji umożliwiające sortowanie kontenera
class ExpensiveObject
{
private:
    char* data_;
    size_t size_;
public:
    ExpensiveObject(const char* data) : data_(NULL), size_(std::strlen(data))
    {
        data_ = new char[size_+1];
        std::strcpy(data_, data);
    }

    ExpensiveObject(const ExpensiveObject& source) : data_(NULL), size_(source.size_)
    {
        data_ = new char[size_+1];
        std::strcpy(data_, source.data_);
    }

    ExpensiveObject(ExpensiveObject&& source) noexcept
        : size_(source.size_)
    {
        data_ = source.data_;
        source.data_ = nullptr;
    }

    ~ExpensiveObject()
    {
        delete [] data_;
    }

    ExpensiveObject& operator=(const ExpensiveObject& source)
    {
        ExpensiveObject temp(source);
        swap(temp);
        return *this;
    }

    ExpensiveObject& operator=(ExpensiveObject&& source) noexcept
    {
        delete [] data_;
        data_ = source.data_;
        size_ = source.size_;
        source.data_ = nullptr;
        return *this;
    }

    void swap(ExpensiveObject& other)
    {
        std::swap(data_, other.data_);
        std::swap(size_, other.size_);
    }

    const char* data() const
    {
        return data_;
    }

    size_t size() const
    {
        return size_;
    }

    bool operator<(const ExpensiveObject& other) const
    {
        if (std::strcmp(data_, other.data_) < 0)
            return true;
        return false;
    }
};

ExpensiveObject expensive_object_generator()
{
    static char txt[] = "abcdefghijklmn";
    static const size_t size = std::strlen(txt);

    std::random_shuffle(txt, txt + size);

    return ExpensiveObject(txt);
}


template <typename Container>
void sort(Container& cont)
{
    sort(begin(cont), end(cont));
}

template <typename T>
void sort(list<T>& cont)
{
    cont.sort();
}


template<typename Cont, typename fun>
void test(Cont cont, fun f, int n)
{
    auto start = chrono::high_resolution_clock::now();
    for(int i = 0 ; i < n ; ++i)
    {
        cont.push_back( f() );
    }
    auto end1 = chrono::high_resolution_clock::now();
    sort(cont);
    auto end2 = chrono::high_resolution_clock::now();
    cout <<  chrono::duration_cast<chrono::milliseconds>(end1-start).count() << " ms, ";
    cout <<  chrono::duration_cast<chrono::milliseconds>(end2-end1).count() << " ms" << endl;
}


int main()
{
    const int n = 1000000;

    vector<long> vec;
    test(vec, [] {return rand()%100;}, n);
    deque<long> deq;
    test(deq, [] {return rand()%100;}, n);
    list<long> lst;
    test(lst, [] {return rand()%100;}, n);

    vector<ExpensiveObject> vec2;
    test(vec2, expensive_object_generator, n);
    deque<ExpensiveObject> deq2;
    test(deq2, expensive_object_generator, n);
    list<ExpensiveObject> lst2;
    test(lst2, expensive_object_generator, n);

}
