#include <iostream>
#include <map>
#include <chrono>
#include <unordered_map>
#include <vector>
#include <algorithm>
#include <fstream>

using namespace std;

int main()
{
    ifstream in("/home/leszek/swanns_way.txt");
    unordered_map<string, int> freq;
    //map<string, int> freq;
    string word;
    auto start = chrono::high_resolution_clock::now();
    while (in >> word)
    {
        freq[move(word)]++;
    }
    /*for(auto& el : freq)
    {
        cout << el.first << " : " << el.second << endl;
    }*/

    auto end1 = chrono::high_resolution_clock::now();
//    multimap<int, string> result;
//    for(auto& el : freq)
//    {
//         result.insert(make_pair(el.second, el.first));
//    }
//    for(auto& el : result)
//    {
//        cout << el.first << " : " << el.second << endl;
//    }


    map<int, vector<string>, greater<int>> result;
    for(auto& el : freq)
    {
        result[el.second].push_back(move(el.first));
    }
    int i{};
    for(auto& el : result)
    {
        i++;
        if (i == 10) break;
        cout << el.first << " : ";
        for(auto& w : el.second)
        {
            cout << w << ", ";
        }
        cout << endl;
    }

    auto end2 = chrono::high_resolution_clock::now();
    cout <<  chrono::duration_cast<chrono::microseconds>(end1-start).count() << " us, ";
    cout <<  chrono::duration_cast<chrono::microseconds>(end2-end1).count() << " us" << endl;

    return 0;
}

