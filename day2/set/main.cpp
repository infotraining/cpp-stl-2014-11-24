#include <iostream>
#include <set>

using namespace std;

int main()
{
    cout << "Seth" << endl;
    set<int, greater<int>> s;

    //s.insert(7);
    s.insert(4);
    s.insert(11);
    for (auto&el : s)
        cout << el << " ";
    cout << endl;
    cout << s.count(1) << endl;
    cout << s.count(4) << endl;;
    cout << endl;

    if (s.insert(7).second)
    {
        cout << "7 was inserted" << endl;
    }
    else
    {
        cout << "already in set" << endl;
    }
    return 0;
}


