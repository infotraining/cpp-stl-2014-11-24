#include <iostream>
#include <fstream>
#include <set>
#include <string>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <chrono>
#include <unordered_set>
#include <vector>
#include <algorithm>
#include <iterator>
using namespace std;

int main()
{
    cout << "Hello spellcheck!" << endl;

    //set<string> dict;
    unordered_set<string> dict;
    cout << "Bucket count " << dict.bucket_count() <<endl;
    cout << "Load Factor " << dict.load_factor()<<endl;
    cout << "max LF " << dict.max_load_factor()<<endl;
    //vector<string> dict;

    ifstream in("/home/leszek/pl.dict.sort");
    string word;
    auto start = chrono::high_resolution_clock::now();
    auto lastit = dict.end();

    dict.reserve(long(3669299.0/0.71));
    //dict.insert(istream_iterator<string>(in), istream_iterator<string>());
    while (in >> word)
    {
        lastit = dict.insert(lastit, move(word));
        //dict.push_back(word);
    }
    cout << "Bucket count " << dict.bucket_count()<<endl;
    cout << "Load Factor " << dict.load_factor()<<endl;
    cout << "max LF " << dict.max_load_factor()<<endl;

    auto end1 = chrono::high_resolution_clock::now();
    //for (auto& w : dict) cout << w << ", ";
    cout << endl;
    string sentence = "this is an example of the sntence";
    vector<string> words {"Ala", "ma", "kota", "abstrakcyjy", "Kazimierz"};
    //boost::split(words, sentence, boost::is_any_of(" "));

    for(auto& w : words)
    {
        if (!dict.count(w))
        //if ( count(dict.begin(), dict.end(), w) == 0)
        {
            cout << w << " is misspelled" << endl;
        }
    }
    auto end2 = chrono::high_resolution_clock::now();
    cout <<  chrono::duration_cast<chrono::milliseconds>(end1-start).count() << " ms, ";
    cout <<  chrono::duration_cast<chrono::microseconds>(end2-end1).count() << " us" << endl;
    return 0;
}

